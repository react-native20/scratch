import { create } from "tailwind-rn";
import styles from "../../styles.json";

const { tailwind, getColor } = create(styles);
export { tailwind, getColor };

// const { letterSpacing } = require('tailwindcss/defaultTheme')
// letterSpacing: {
//   ...letterSpacing,
//   tightest: '-0.125rem',
// },
