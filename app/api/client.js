import { create } from "apisauce";

/**
 *Apisauce Response
 *
 * data - the object originally from the server that you might wanna mess with
 * duration - the number of milliseconds
 * problem - the problem code (see the bottom for the list)
 * ok - true or false
 * status - the HTTP status code
 * headers - the HTTP response headers
 * config - the underlying axios config for the request
 */

export default apiClient = create({
  baseURL: "http://192.168.1.133:3000/api/v1",
  headers: { Accept: "application/json" },
});
