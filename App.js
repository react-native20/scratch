import React, { useState } from "react";
import { StatusBar } from "expo-status-bar";

import AppNavigation from "./app/navigation/AppNavigation";
import ThemeContextProvider from "./app/context/ThemeContext";
import AppContext from "./app/context";

const initAppContext = {
  // @todo use fold id instead
  activeFolder: false,
};

export default function App() {
  const [appContext, setAppContext] = useState(initAppContext);

  return (
    <AppContext.Provider value={{ appContext, setAppContext }}>
      <ThemeContextProvider>
        <StatusBar style="light" backgroundColor="rgba(232, 176, 3, 0.5)" />
        <AppNavigation />
      </ThemeContextProvider>
    </AppContext.Provider>
  );
}
