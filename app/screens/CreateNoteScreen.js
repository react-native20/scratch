import React, { useState, useLayoutEffect } from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Alert,
  TouchableNativeFeedback,
} from "react-native";
import { tailwind } from "../lib/tailwind";

import routes from "../navigation/routes";
import Screen from "../components/Screen";
import useTheme from "../hooks/useTheme";
import colors from "../styles/colors";
import Divider from "../components/Divider";
import apiClient from "../api/client";

export default function CreateNoteScreen({ navigation, route }) {
  const folder = route.params?.folder || null;
  const [title, setTitle] = useState(null);
  const [body, setBody] = useState(null);
  const { theme } = useTheme();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={{ marginHorizontal: 15 }}>
          <MaterialCommunityIcons
            color={theme.colors.primary}
            name="check"
            size={30}
            onPress={saveNote}
          />
        </View>
      ),
      headerLeft: () => (
        <View style={{ marginHorizontal: 15 }}>
          <TouchableNativeFeedback>
            <MaterialCommunityIcons
              color={theme.colors.primary}
              name="arrow-left"
              size={27}
              onPress={saveNote}
            />
          </TouchableNativeFeedback>
        </View>
      ),
    });
  }, [title, body]);

  const saveNote = async () => {
    const note = { title, body };
    if (folder) {
      note.folder_id = folder?.id;
    }
    const response = await apiClient.post("/notes", note);
    if (response.ok) {
      // if a folder was passed to the screen,
      // navigate back to that folder, else, navigate
      // to notes screen
      if (folder) {
        navigation.navigate(routes.FOLDER_NOTES, { folder });
      } else {
        navigation.navigate(routes.NOTES);
      }
    } else {
      Alert.alert("Error", "Something went wrong.");
    }
  };

  return (
    <Screen>
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="Note Title"
            placeholderTextColor={
              theme.isLight ? colors.gray : colors.mediumGray
            }
            style={[
              styles.title,
              { color: theme.isLight ? colors.darkGray : colors.gray },
            ]}
            defaultValue={title}
            onChangeText={(text) => setTitle(text)}
          />
        </View>
        <Divider />
        <View style={styles.section}>
          <TextInput
            placeholder="Start writing here"
            placeholderTextColor={
              theme.isLight ? colors.gray : colors.mediumGray
            }
            style={[
              styles.body,
              { color: theme.isLight ? colors.darkGray : colors.gray },
            ]}
            defaultValue={body}
            onChangeText={(text) => setBody(text)}
            multiline
            numberOfLines={4}
          />
        </View>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  actions: tailwind("flex flex-row justify-end"),
  action: tailwind("ml-3"),
  container: tailwind("px-3"),
  section: tailwind("mb-5"),
  title: tailwind("text-lg"),
  body: tailwind("text-lg"),
});
