const NOTES = [
  {
    id: 1,
    title: "Working Hours",
    date: "09 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 2,
    title: "Documents",
    date: "10 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 3,
    title: "App Testing",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 4,
    title: "Continuous Deployment",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 5,
    title: "React Native",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 6,
    title: "Continuous Deployment",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 7,
    title: "Continuous Deployment",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 8,
    title: "Continuous Deployment",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 9,
    title: "Continuous Deployment",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
  {
    id: 10,
    title: "Continuous Deployment",
    date: "11 Sept 2020",
    body:
      "Lorem ipsum dolor sit amet, consectetur adi piscing elit, sed doeiusmod tempor incididunt.",
  },
];

export default {
  NOTES,
};
