import React, { createContext, useState } from "react";
import { NavigationContainer } from "@react-navigation/native";

import { lightAppTheme, darkAppTheme } from "../styles/appTheme";

export const ThemeContext = createContext();

export default ThemeContextProvider = ({ children }) => {
  const [theme, setTheme] = useState(darkAppTheme);

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      <NavigationContainer theme={theme.navigation}>
        {children}
      </NavigationContainer>
    </ThemeContext.Provider>
  );
};
