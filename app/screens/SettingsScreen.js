import React, { useState } from "react";
import { StyleSheet, View, Switch } from "react-native";

import Screen from "../components/Screen";
import { lightAppTheme, darkAppTheme } from "../styles/appTheme";
import Text from "../components/Text";
import useTheme from "../hooks/useTheme";
import colors from "../styles/colors";

export default function SettingsScreen(props) {
  const { theme, setTheme } = useTheme();
  const [isEnabled, setIsEnabled] = useState(true);
  const changeTheme = () => {
    const newTheme = isEnabled ? lightAppTheme : darkAppTheme;
    setTheme(newTheme);
    setIsEnabled((prevState) => !prevState);
  };

  return (
    <Screen style={styles.container}>
      <Text style={styles.screenTitle}>Settings</Text>
      <View style={styles.setting}>
        <View>
          <Text style={styles.settingTitle}>Dark Mode</Text>
        </View>
        <View>
          <Switch
            trackColor={{ false: "#767577", true: theme.colors.primary }}
            thumbColor={isEnabled ? "#f4f3f4" : "#f4f3f4"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={changeTheme}
            value={isEnabled}
          />
        </View>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  screenTitle: {
    paddingBottom: 20,
    fontSize: 20,
    color: colors.primary,
  },
  setting: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  settingTitle: {
    fontSize: 16,
  },
});
