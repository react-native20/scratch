import React, { useEffect, useContext, useState } from "react";
import { StyleSheet, View, ScrollView, Button, TextInput } from "react-native";
import { tailwind } from "../lib/tailwind";
import { FloatingAction } from "react-native-floating-action";

import Screen from "../components/Screen";
import colors from "../styles/colors";
import Text from "../components/Text";
import Folder from "../components/Folder";
import routes from "../navigation/routes";
import AppContext from "../context";
import useTheme from "../hooks/useTheme";
import { useIsFocused } from "@react-navigation/native";
import client from "../api/client";
import CreateFolderModal from "../components/CreateFolderModal";

const actions = [
  {
    text: "New Folder",
    name: "new_folder",
    icon: require("../assets/images/new-note.png"),
    color: colors.primary,
    position: 1,
  },
];

export default function FoldersScreen({ navigation }) {
  const [folders, setFolders] = useState([]);
  const [renderTrigger, setRenderTrigger] = useState(0);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const { theme } = useTheme();
  const isFocused = useIsFocused();

  useEffect(() => {
    fetchFolders();
  }, [isFocused, renderTrigger]);

  const fetchFolders = async () => {
    const response = await client.get("/folders");
    if (response.ok) {
      setFolders(response.data);
      setRenderTrigger(0);
    }
  };

  const handleFolderPress = (folder) => {
    navigation.navigate(routes.FOLDER_NOTES, { folder });
  };

  const handleActionPress = (action) => {
    switch (action) {
      case "new_folder":
        setIsModalVisible(true);
    }
  };

  // @todo use a flatList instead
  const foldersList =
    folders && folders.length ? (
      folders.map((item) => (
        <Folder
          key={item.id}
          title={item.title}
          noteCount={10}
          onPress={() => handleFolderPress(item)}
        />
      ))
    ) : (
      <Text>You have'nt created any folders yet.</Text>
    );

  return (
    <Screen style={styles.container}>
      <ScrollView>{foldersList}</ScrollView>
      <FloatingAction
        actions={actions}
        color={theme.colors.primary}
        dismissKeyboardOnPress
        distanceToEdge={7}
        onPressItem={handleActionPress}
      />
      <CreateFolderModal
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        setRenderTrigger={setRenderTrigger}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    paddingBottom: 2,
  },
});
