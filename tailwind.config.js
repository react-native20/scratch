// const { letterSpacing } = require("tailwindcss/defaultTheme");

module.exports = {
  purge: [],
  theme: {
    extend: {
      letterSpacing: {
        tighter: "-0.05rem",
        tight: "-0.025rem",
        normal: "0",
        wide: "0.025rem",
        wider: "0.05rem",
        widest: "0.1rem",
      },
    },
  },
  variants: {},
  plugins: [],
};
