import React from "react";
import { View } from "react-native";

import { tailwind } from "../lib/tailwind";
import colors from "../styles/colors";

export default function Divider() {
  return (
    <View
      style={[
        tailwind("mb-6 border-b"),
        {
          borderColor: colors.mediumGray,
        },
      ]}
    />
  );
}
