import React from "react";
import { SafeAreaView, StyleSheet, View } from "react-native";
import Constants from "expo-constants";

import useTheme from "../hooks/useTheme";

export default function Screen({ children, style }) {
  const { theme } = useTheme();

  return (
    <SafeAreaView style={[styles.screen, style, { backgroundColor: theme.bg }]}>
      <View style={style}>{children}</View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
  },
  view: {
    flex: 1,
  },
});
