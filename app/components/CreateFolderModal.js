import React, { useState } from "react";
import { StyleSheet, View, TextInput, Button, Alert } from "react-native";
import Modal from "react-native-modal";
import client from "../api/client";

import colors from "../styles/colors";

export default function CreateFolderModal({
  isModalVisible,
  setIsModalVisible,
  setRenderTrigger,
}) {
  const [title, setTitle] = useState(null);

  const saveFolder = async () => {
    const response = await client.post("/folders", { title });
    if (response.ok) {
      setIsModalVisible(false);
      setRenderTrigger(1);
    } else {
      Alert.alert("Error", "Something went wrong saving folder.");
    }
  };

  return (
    <Modal
      isVisible={isModalVisible}
      coverScreen={false}
      backdropColor={colors.black}
      backdropOpacity={1}
      onBackdropPress={() => setIsModalVisible(false)}
    >
      <View style={{ flex: 1 }}>
        <TextInput
          autoFocus
          style={{ fontSize: 20, marginBottom: 40, color: colors.lightGray }}
          keyboardAppearance="dark"
          placeholder="Folder Title"
          placeholderTextColor="gray"
          defaultValue={title}
          onChangeText={setTitle}
        />
        <Button
          title="Create Folder"
          color={colors.primary}
          onPress={saveFolder}
        />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {},
});
