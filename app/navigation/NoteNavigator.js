import React from "react";
import { Button, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";

import NotesScreen from "../screens/NotesScreen";
import NoteScreen from "../screens/NoteScreen";
import useTheme from "../hooks/useTheme";
import CreateNoteScreen from "../screens/CreateNoteScreen";

const Stack = createStackNavigator();

export default NoteNavigator = () => {
  const { theme } = useTheme();
  return (
    <Stack.Navigator
      mode="modal"
      screenOptions={{
        gestureEnabled: true,
        headerShown: true,
        headerTintColor: theme.colors.primary,
        headerStyle: { backgroundColor: theme.ui },
      }}
    >
      <Stack.Screen
        name="Notes"
        component={NotesScreen}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="Note"
        component={NoteScreen}
        options={{
          headerTitle: null,
        }}
      />
      <Stack.Screen
        name="AddNote"
        component={CreateNoteScreen}
        options={{ headerTitle: "New Note" }}
      />
    </Stack.Navigator>
  );
};
