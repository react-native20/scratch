export default Object.freeze({
  NOTES: "Notes",
  NOTE: "Note",
  ADD_NOTE: "AddNote",
  EDIT_NOTE: "EditNote",
  SETTINGS: "Settings",
  FOLDERS: "Folders",
  FOLDER_NOTES: "FolderNotes",
  FOLDER_NOTE: "FolderNote",
});
