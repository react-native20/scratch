import React, { useState, useLayoutEffect } from "react";
import { Button, StyleSheet, View, TextInput, Alert } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { StackActions } from "@react-navigation/native";

import Divider from "../components/Divider";
import Screen from "../components/Screen";
import { tailwind } from "../lib/tailwind";
import routes from "../navigation/routes";
import useTheme from "../hooks/useTheme";
import colors from "../styles/colors";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import client from "../api/client";

export default function NoteScreen({ navigation, route }) {
  const note = route.params;
  const [title, setTitle] = useState(note.title);
  const [body, setBody] = useState(note.body);
  const { theme } = useTheme();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View style={{ marginHorizontal: 15 }}>
          <TouchableNativeFeedback>
            <MaterialCommunityIcons
              color={theme.colors.primary}
              name="arrow-left"
              size={27}
              onPress={saveNote}
            />
          </TouchableNativeFeedback>
        </View>
      ),
      headerRight: () => (
        <View style={{ marginHorizontal: 15 }}>
          <MaterialCommunityIcons
            color={theme.colors.primary}
            name="check"
            size={30}
            onPress={saveNote}
          />
        </View>
      ),
    });
  }, [navigation, title, body]);

  const saveNote = async () => {
    const result = await client.patch(`/notes/${note.id}`, {
      title,
      body,
    });

    if (result.ok) {
      navigation.navigate(routes.NOTES);
    } else {
      Alert.alert("Error", "Something went wrong updating the note.");
    }
  };

  return (
    <Screen>
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="Note Title"
            placeholderTextColor={
              theme.isLight ? theme.colors.gray : theme.colors.mediumGray
            }
            style={[
              styles.title,
              {
                color: theme.isLight
                  ? theme.colors.darkGray
                  : theme.colors.gray,
              },
            ]}
            defaultValue={title}
            onChangeText={(value) => setTitle(value)}
          />
        </View>
        <Divider />
        <View style={styles.section}>
          <TextInput
            placeholder="Start writing here"
            placeholderTextColor={
              theme.isLight ? theme.colors.gray : theme.colors.mediumGray
            }
            style={[
              styles.body,
              {
                color: theme.isLight
                  ? theme.colors.darkGray
                  : theme.colors.gray,
              },
            ]}
            defaultValue={body}
            onChangeText={(value) => setBody(value)}
            multiline
            numberOfLines={4}
          />
        </View>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: tailwind("px-3"),
  section: tailwind("mb-5"),
  title: tailwind("text-lg"),
  body: tailwind("text-lg"),
});
