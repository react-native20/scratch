export default {
  primary: "rgb(232, 176, 3)",

  white: "#ffffff",
  black: "#000",

  lightGray: "#f8f8f8",
  gray: "#b3b3b3",
  mediumGray: "#555",
  darkGray: "#262626",

  darkBg: "#121212",
  darkUi: "#1d1d1d",
  darkButton: "#292929",
  darkLightest: "#363636",

  lightBg: "#f8f8f8",
};
