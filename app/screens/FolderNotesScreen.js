import React, { useContext, useEffect, useState } from "react";
import { StyleSheet, View, FlatList, Alert } from "react-native";
import { tailwind } from "../lib/tailwind";
import { FloatingAction } from "react-native-floating-action";
import { useIsFocused, useNavigation } from "@react-navigation/native";

import Note from "../components/Note";
import Screen from "../components/Screen";
import Text from "../components/Text";
import routes from "../navigation/routes";
import useTheme from "../hooks/useTheme";
import colors from "../styles/colors";
import AppContext from "../context";
import client from "../api/client";

const actions = [
  {
    text: "New Folder",
    name: "new_folder",
    icon: require("../assets/images/new-note.png"),
    color: colors.primary,
    position: 1,
  },
  {
    text: "New Note",
    name: "new_note",
    icon: require("../assets/images/new-note.png"),
    color: colors.primary,
    position: 2,
  },
];

export default function FolderNotesScreen({ navigation, route }) {
  const { folder } = route.params;
  const [notes, setNotes] = useState(false);
  const { theme } = useTheme();
  const nav = useNavigation();
  const { appContext, setAppContext } = useContext(AppContext);
  const isFocused = useIsFocused();

  useEffect(() => {
    fetchNotes();
  }, [isFocused]);

  const fetchNotes = async () => {
    const response = await client.get(`/folders/${folder.id}`);
    if (response.ok) {
      setNotes(response.data.notes);
    }
  };

  const handleActionPress = (action) => {
    switch (action) {
      case "new_note":
        nav.navigate(routes.ADD_NOTE, { folder });
    }
  };

  return (
    <Screen style={[styles.container, { backgroundColor: theme.bg }]}>
      <FlatList
        ListEmptyComponent={() => (
          <Text style={{ flex: 1 }}>No notes available</Text>
        )}
        data={notes}
        keyExtractor={(note) => note.id.toString()}
        renderItem={({ item }) => (
          <Note
            title={item.title}
            date={item.createdAt}
            body={item.body}
            onPress={() => navigation.navigate(routes.FOLDER_NOTE, item)}
          />
        )}
      />
      <FloatingAction
        actions={actions}
        color={theme.colors.primary}
        dismissKeyboardOnPress
        distanceToEdge={0}
        mainVerticalDistance={10}
        onPressItem={handleActionPress}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: tailwind("pt-0 px-1"),
});
