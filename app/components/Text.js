import React from "react";
import { Text } from "react-native";

import useTheme from "../hooks/useTheme";
import { tailwind } from "../lib/tailwind";

export default function AppText({ style, children }) {
  const { theme } = useTheme();
  const textStyles = theme.isLight
    ? tailwind("text-gray-700")
    : tailwind("text-gray-500");

  return <Text style={[textStyles, style]}>{children}</Text>;
}
