import { DefaultTheme, DarkTheme } from "@react-navigation/native";
import colors from "../styles/colors";

export const lightTheme = {
  ...DefaultTheme,
  dark: false,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary,
    text: "gray",
    background: "white",
  },
};
export const darkTheme = {
  ...DarkTheme,
  dark: true,
  colors: {
    ...DarkTheme.colors,
    primary: colors.primary,
    text: "gray",
    background: "#ddd",
  },
};
