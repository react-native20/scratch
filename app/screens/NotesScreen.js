import React, { useEffect, useContext, useState } from "react";
import { StyleSheet, View, FlatList, Alert } from "react-native";
import { tailwind } from "../lib/tailwind";
import { FloatingAction } from "react-native-floating-action";

import Note from "../components/Note";
import Screen from "../components/Screen";
import Text from "../components/Text";
import routes from "../navigation/routes";
import useTheme from "../hooks/useTheme";
import data from "../config/data";
import colors from "../styles/colors";
import { useNavigation, useIsFocused } from "@react-navigation/native";
import apiClient from "../api/client";

const actions = [
  {
    text: "New Note",
    name: "new_note",
    icon: require("../assets/images/new-note.png"),
    color: colors.primary,
    position: 2,
  },
];

export default function NotesScreen({ navigation }) {
  const [notes, setNotes] = useState([]);
  const { theme } = useTheme();
  const nav = useNavigation();
  const isFocused = useIsFocused();

  useEffect(() => {
    fetchNotes();
  }, [isFocused]);

  const fetchNotes = async () => {
    const response = await apiClient.get("/notes");
    if (response.ok) {
      setNotes(response.data);
    }
  };

  const handleActionPress = (action) => {
    switch (action) {
      case "new_note":
        nav.navigate(routes.ADD_NOTE);
    }
  };

  return (
    <Screen style={[styles.container, { flex: 1, backgroundColor: theme.bg }]}>
      <Text
        style={[
          styles.screenTitle,
          { color: theme.isLight ? "gray" : theme.colors.primary },
        ]}
      >
        All Notes
      </Text>
      <FlatList
        data={notes}
        keyExtractor={(note) => note.id.toString()}
        renderItem={({ item }) => (
          <Note
            title={item.title}
            date={item.createdAt}
            body={item.body}
            onPress={() => navigation.navigate(routes.NOTE, item)}
          />
        )}
      />
      <FloatingAction
        actions={actions}
        color={theme.colors.primary}
        dismissKeyboardOnPress
        distanceToEdge={0}
        mainVerticalDistance={60}
        onPressItem={handleActionPress}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: tailwind("px-1"),
  screenTitle: tailwind("py-3 px-2 text-lg font-bold"),
});
