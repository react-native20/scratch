import React, { useContext } from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { StyleSheet, View, TouchableOpacity, Alert } from "react-native";
import { tailwind } from "../lib/tailwind";

import Text from "./Text";

export default function Folder({ title, noteCount = 0, onPress }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={tailwind(
        "flex flex-row justify-between py-4 px-4 border-b border-gray-800"
      )}
    >
      <View>
        <Text style={tailwind("mb-1 text-lg font-bold")}>{title}</Text>
        <Text style={tailwind("text-xs")}>{noteCount} Notes</Text>
      </View>
      <TouchableOpacity onPress={() => Alert.alert("test", "test modal")}>
        <MaterialCommunityIcons
          name="settings-helper"
          size={40}
          color="gray"
          style={{
            marginRight: -30,
            transform: [{ rotateZ: "90deg" }],
          }}
        />
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {},
});
