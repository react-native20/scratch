import React from "react";
import { Button, View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";

import NotesScreen from "../screens/NotesScreen";
import useTheme from "../hooks/useTheme";
import CreateNoteScreen from "../screens/CreateNoteScreen";
import FoldersScreen from "../screens/FoldersScreen";
import FolderNotesScreen from "../screens/FolderNotesScreen";
import FolderNoteScreen from "../screens/FolderNoteScreen";

const Stack = createStackNavigator();

export default FolderNavigator = () => {
  const { theme } = useTheme();
  return (
    <Stack.Navigator
      mode="modal"
      screenOptions={{
        gestureEnabled: true,
        headerShown: true,
        headerTintColor: theme.colors.primary,
        headerStyle: { backgroundColor: theme.ui },
      }}
    >
      <Stack.Screen name="Folders" component={FoldersScreen} />
      <Stack.Screen
        name="FolderNotes"
        component={FolderNotesScreen}
        options={({ route }) => ({
          title: route.params.folder?.title,
        })}
      />
      <Stack.Screen
        name="FolderNote"
        component={FolderNoteScreen}
        options={{
          title: null,
        }}
      />
    </Stack.Navigator>
  );
};
