import { useContext } from "react";
import { ThemeContext } from "../context/ThemeContext";

export default useTheme = () => {
  return useContext(ThemeContext);
};
