import { lightTheme, darkTheme } from "../navigation/navigationTheme";
import colors from "./colors";

export const lightAppTheme = {
  isLight: true,
  bg: colors.lightBg,
  ui: colors.white,
  navigation: lightTheme,
  colors,
};

export const darkAppTheme = {
  isLight: false,
  bg: colors.darkBg,
  ui: colors.darkUi,
  navigation: darkTheme,
  colors,
};
