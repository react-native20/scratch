import React from "react";
import { StyleSheet, View, TouchableWithoutFeedback } from "react-native";
import { tailwind } from "../lib/tailwind";

import useTheme from "../hooks/useTheme";
import Text from "../components/Text";

export default function Note({ title, date, body, onPress }) {
  const { theme } = useTheme();

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.container, { backgroundColor: theme.ui }]}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.date}>{date}</Text>
        <Text>{body}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: tailwind("p-6 mt-3 rounded-lg"),
  title: tailwind("font-bold text-base mb-1"),
  date: tailwind("mb-4 text-gray-600"),
});
