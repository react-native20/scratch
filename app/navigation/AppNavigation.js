import React from "react";
import { StyleSheet, View, Alert } from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  MaterialCommunityIcons,
  Feather,
  FontAwesome,
} from "@expo/vector-icons";

import CreateNoteScreen from "../screens/CreateNoteScreen";
import NoteNavigator from "./NoteNavigator";
import CreateNoteButton from "./CreateNoteButton";
import SettingsScreen from "../screens/SettingsScreen";
import FoldersScreen from "../screens/FoldersScreen";
import routes from "./routes";
import data from "../config/data";
import useTheme from "../hooks/useTheme";
import FolderNavigator from "./FolderNavigator";

const Tab = createBottomTabNavigator();

export default function AppNavigation(props) {
  const { theme } = useTheme();
  return (
    <Tab.Navigator
      screenOptions={({ route }) => {
        return {
          tabBarIcon: ({ focused, color, size }) => {},
        };
      }}
      tabBarOptions={{
        activeTintColor: theme.colors.primary,
        inactiveTintColor: "lightgray",
        style: {
          paddingBottom: 4,
        },
      }}
    >
      <Tab.Screen
        name="Notes"
        component={NoteNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <FontAwesome name="sticky-note" color={color} size={20} />
          ),
        }}
      />
      <Tab.Screen
        name="Folders"
        component={FolderNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
              name="account-tie"
              color={color}
              size={20}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          tabBarIcon: ({ color, size }) => (
            <Feather name="settings" color={color} size={20} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
