import React from "react";
import { StyleSheet } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { TouchableOpacity } from "react-native-gesture-handler";

import useTheme from "../hooks/useTheme";

export default function CreateNoteButton({ onPress }) {
  const {
    theme: { colors },
  } = useTheme();
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, { backgroundColor: colors.primary }]}
    >
      <MaterialCommunityIcons name="plus-circle" color="white" size={40} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    borderColor: "lightgray",
    borderWidth: 0,
    borderRadius: 40,
    bottom: 35,
    height: 80,
    width: 80,
  },
});
