import React, { useState, useLayoutEffect } from "react";
import { Button, StyleSheet, View, TextInput } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { StackActions } from "@react-navigation/native";

import Divider from "../components/Divider";
import Screen from "../components/Screen";
import { tailwind } from "../lib/tailwind";
import routes from "../navigation/routes";
import useTheme from "../hooks/useTheme";
import colors from "../styles/colors";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import client from "../api/client";

export default function FolderNoteScreen({ navigation, route }) {
  const note = route.params;
  const [title, setTitle] = useState(note.title);
  const [body, setBody] = useState(note.body);
  const [hasUnsavedChanges, setHasUnsavedChanges] = useState(false);
  const { theme } = useTheme();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View style={{ marginHorizontal: 15 }}>
          <TouchableNativeFeedback>
            <MaterialCommunityIcons
              color={theme.colors.primary}
              name="arrow-left"
              size={27}
              onPress={saveNote}
            />
          </TouchableNativeFeedback>
        </View>
      ),
      headerRight: () => (
        <View style={{ marginHorizontal: 15 }}>
          <MaterialCommunityIcons
            color={theme.colors.primary}
            name="check"
            size={30}
            onPress={saveNote}
          />
        </View>
      ),
    });
  }, [navigation, title, body]);

  const handleTitleChange = (value) => {
    setHasUnsavedChanges(true);
    setTitle(value);
  };

  const handleBodyChange = (value) => {
    setHasUnsavedChanges(true);
    setBody(value);
  };

  const saveNote = async () => {
    const response = await client.patch(`/notes/${note.id}`, { title, body });
    if (response.ok) {
      navigation.goBack();
    } else {
      Alert.alert("Error", "Something went wrong updating the note.");
    }
  };

  return (
    <Screen>
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="Note Title"
            placeholderTextColor={
              theme.isLight ? theme.colors.gray : theme.colors.mediumGray
            }
            style={[
              styles.title,
              {
                color: theme.isLight
                  ? theme.colors.darkGray
                  : theme.colors.gray,
              },
            ]}
            value={title}
            onChangeText={handleTitleChange}
          />
        </View>
        <Divider />
        <View style={styles.section}>
          <TextInput
            placeholder="Start writing here"
            placeholderTextColor={
              theme.isLight ? theme.colors.gray : theme.colors.mediumGray
            }
            style={[
              styles.body,
              {
                color: theme.isLight
                  ? theme.colors.darkGray
                  : theme.colors.gray,
              },
            ]}
            value={body}
            onChangeText={handleBodyChange}
            multiline
            numberOfLines={4}
          />
        </View>
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: tailwind("px-3"),
  section: tailwind("mb-5"),
  title: tailwind("text-lg"),
  body: tailwind("text-lg"),
});
